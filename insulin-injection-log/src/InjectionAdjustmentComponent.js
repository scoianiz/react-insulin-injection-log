import React from 'react';
import * as Buttons from './ButtonComponents';
import * as TextArea from './TextAreaComponents';
import './InjectionAdjustmentComponents.css'

export function ShortActingAdjustment(){
    return (<div className="InjectionAdjustment">
        <Buttons.IncreaseButton/>
        <TextArea.InjectionDose/>
        <Buttons.DecreaseButton/>
    </div>);
}

export function LongActingAdjustment(){
    return (<div className="InjectionAdjustment">
        <Buttons.IncreaseButton/>
        <TextArea.InjectionDose/>
        <Buttons.DecreaseButton/>
    </div>);
}