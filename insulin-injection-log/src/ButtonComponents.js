import React from 'react';
import './ButtonComponent.css';

export function DecreaseButton(){
    return (<button type="button" className="InButton Decrease">-</button>);
}
export function IncreaseButton(){
    return (<button type="button" className="InButton Increase">+</button>);
}

export function LogInjectionButton(){
    return (<button type="button" className="InButton LogInjection">Log injection</button>)
}