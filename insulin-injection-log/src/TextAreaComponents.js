import React from "react";
import "./TextAreaComponents.css";

export function InjectionDose() {
  return <input type="text" className="InjectionInput" />;
}

export function MostRecentInjectionLog() {
  return <div className="MostRecentLog" />;
}

export function AreaHeading(props) {
  return <div className="AreaHeading">{props.value}</div>;
}
