import React from "react";
import "./App.css";
import "./ButtonComponents";
import "./ButtonComponents";
import * as InjectionComponents from "./FullInjectionLogComponent";

function App() {
  return (
    <div className="App">
      <header className="App-header">

        <InjectionComponents.FullLongActingComponent />
        <br></br>
        <InjectionComponents.FullShortActingComponent />
      </header>
    </div>
  );
}

export default App;
