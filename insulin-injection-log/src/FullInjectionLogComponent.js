import React from "react";
import * as TextAreas from "./TextAreaComponents";
import * as LogComponents from "./InjectionAdjustmentComponent";
import "./FullInjectionLogComponent.css";

export function FullShortActingComponent() {
  return (
    <div className="FullShortActing">
      <div className="DisplayColumn">
        <TextAreas.AreaHeading value={"Short Acting Insulin"} />
        <TextAreas.MostRecentInjectionLog />
      </div>
      <LogComponents.ShortActingAdjustment />
    </div>
  );
}

export function FullLongActingComponent() {
  return (
    <div className="FullLongActing">
      <div className="DisplayColumn">
        <TextAreas.AreaHeading value={"Long Acting Insulin"} />
        <TextAreas.MostRecentInjectionLog />
      </div>
        <LogComponents.LongActingAdjustment />
    </div>
  );
}
